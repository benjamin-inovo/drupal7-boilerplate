#Drupal7

##Overview
This is a fully functional Drupal7 boilerplate project based on Bootstrap SASS with the minimum necessary modules like : Development tools, Backup and Migrate (usefull for updating production), Views, Webform, XML Sitemap (full list here under).

##Installation
 - Fork 'drupal7' project on bitbucket
 - clone newly created repository : 
```
git clone https://username@bitbucket.org/username/MY_FORK.git
./dev/setup.sh
```

###SASS, Compass & Grunt (only in Development Environment)
 - Install rvm, npm, ruby, compass and grunt if needed (see section below)
 - copy SUB_THEME folder and rename .info file : 
```
cp sites/all/themes/SUB_THEME sites/all/themes/MY_FORK
mv sites/all/themes/MY_FORK/SUB_THEME.info sites/all/themes/MY_FORK/MY_FORK.info
```
> update Bootstrap SASS version in  `sites/all/themes/MY_FORK/bootstrap` at your own risks  
> if you definitely want to do that, you will have to do the same in `sites/all/themes/webstar/bootstrap` and you will probably need to launch a `compass watch sites/all/themes/webstar/` to recompile base files
 - Compass
```
cd sites/all/themes/MY_FORK
compass init
compass watch
```
 - npm
``` 
cd sites/all/themes/MY_FORK
npm install
```
 - Grunt : When you are ready to deploy to production
> Modify the 'src' option in your `sites/all/themes/MY_FORK/Gruntfile.js` to match your website's address
```
cd sites/all/themes/MY_FORK
grunt critical --force
```
 - Optional : add bootswatch theme or other : 
```
wget http://bootswatch.com/simplex/_bootswatch.scss -P sites/all/themes/MY_FORK/scss/; \
wget http://bootswatch.com/simplex/_variables.scss  -P sites/all/themes/MY_FORK/bootstrap/assets/stylesheets/bootstrap/ -O sites/all/themes/MY_FORK/bootstrap/assets/stylesheets/bootstrap/_variables.scss
```
> uncomment line `@import 'bootswatch'` in `sites/all/themes/MY_FORK/scss/style.scss`

###MySQL DB
 - Create Database : 
```
create database MY_FORK CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'drupalroot'@'%' IDENTIFIED BY 'YOUR_PASSWORD';
GRANT ALL PRIVILEGES ON MY_FORK.* TO 'drupalroot'@'%' WITH GRANT OPTION;
```
 - Restore : 
```
mysql -u drupalroot -p MY_FORK < ./sites/backup/drupal7-full.sql
```
> *Backup & Restore*
>
> Full backup :  
> ```
> mysqldump -u drupalroot -p MY_FORK > ./sites/backup/MY_FORK-full.sql  
> ```
>
> Structure only Backup & Restore :  
> ```
> mysqldump -u drupalroot -p --no-data MY_FORK > ./sites/backup/MY_FORK-nodata.sql  
> mysql -u drupalroot -p MY_FORK < ./sites/backup/MY_FORK-nodata.sql
> ```

###Finalize Installation
 - File System : 
```
mkdir sites/default/files; \
sudo chgrp www-data sites/default/files; \
cp ./sites/default/default.settings.php sites/default/settings.php; \
sudo chgrp www-data sites/default/settings.php; \
chmod 664 sites/default/settings.php
```
 
 - browse to http://yourserver/MY_FORK/ and follow installation  
 - **After install.php :**
```
chmod 644 ./sites/default/settings.php
```
 
 - Activate your Sub Theme
 
 - Install [drush](http://www.drush.org/en/master/install/)
> update enabled modules and themes : `drush up`

###First deployment to production
> If you already have a deployed website, temporarily modify caching as explained here after in 'Following Deployments to Production'
 
 - Clone sources & install DB
```
git clone https://username@bitbucket.org/username/MY_FORK.git
create database MY_FORK CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'drupalroot'@'%' IDENTIFIED BY 'YOUR_PASSWORD';
GRANT ALL PRIVILEGES ON MY_FORK.* TO 'drupalroot'@'%' WITH GRANT OPTION;
mysql -u drupalroot -p MY_FORK < ./sites/backup/drupal7-full.sql
```
 - Configure Website
```
cp ./sites/default/default.settings.php sites/default/settings.php; \
sudo chgrp www-data sites/default/settings.php; \
chmod 664 sites/default/settings.php
```
 - modify following lines in 'sites/default/settings.php'
```
# $conf['DEV'] = TRUE;
$base_url = 'http://www.YOUR_WEBSITE.com';  // NO trailing slash!
```
 - browse to http://www.YOUR_WEBSITE.com/install.php and follow installation  
 - **After install.php :**
```
chmod 644 ./sites/default/settings.php
```
 - connect to your website and change drupalroot password
 - modify caching values in Home » Administration » Configuration » Development

###Following Deployments to Production
 - Augment caching time according to deployment duration process
 - retrieve data
 ```
 git pull
 ```
 - update database : 
 ```
 mysql -u drupalroot -p MY_FORK < ./sites/backup/MY_FORK-full.sql
 ```

###Usage Notes
Be sure to restore the database to get predefined content types and other features. Also create your sub theme as explained above.
Then create a content based on Column (Bootstrap), assign a row and column number and see the magic happening !  
> to keep the bootstrap logic (rows, columns) you need to use sort criterias in your views (row nb asc, col nb asc) accordingly to your content

###Credentials
base username and password are both 'drupalroot'

---

##Developers Informations 

###Modules
> path : ./sites/all/modules/

Locale (activate)  
Chaos Tool Suite (https://www.drupal.org/project/ctools)  
Development (https://www.drupal.org/project/devel)  
Backup and Migrate (https://www.drupal.org/project/backup_migrate)  
Token (https://www.drupal.org/project/token)  
jQuery update (https://www.drupal.org/project/jquery_update)  
Views (view UI) (https://www.drupal.org/project/views)  
Webform (https://www.drupal.org/project/webform)  
XML Sitemap (menu) (https://www.drupal.org/project/xmlsitemap)  
Conditional Stylesheets (https://www.drupal.org/project/conditional_styles)  
Metatag (https://www.drupal.org/project/metatag)
Advanced CSS/JS Aggregation (https://www.drupal.org/project/advagg)
References (https://www.drupal.org/project/references)
```
wget \
https://ftp.drupal.org/files/projects/ctools-7.x-1.12.tar.gz \
https://ftp.drupal.org/files/projects/devel-7.x-1.5.tar.gz \
https://ftp.drupal.org/files/projects/backup_migrate-7.x-3.1.tar.gz \
https://ftp.drupal.org/files/projects/token-7.x-1.7.tar.gz \
https://ftp.drupal.org/files/projects/jquery_update-7.x-2.7.tar.gz \
https://ftp.drupal.org/files/projects/views-7.x-3.14.tar.gz \
https://ftp.drupal.org/files/projects/webform-7.x-4.14.tar.gz \
https://ftp.drupal.org/files/projects/xmlsitemap-7.x-2.3.tar.gz \
https://ftp.drupal.org/files/projects/conditional_styles-7.x-2.2.tar.gz \
https://ftp.drupal.org/files/projects/metatag-7.x-1.21.tar.gz \
https://ftp.drupal.org/files/projects/advagg-7.x-2.20.tar.gz \
ls *.gz | xargs -i tar zxvf {}
```

###Themes
> path : ./sites/all/themes/

**Bootstrap Theme**
```
wget https://ftp.drupal.org/files/projects/bootstrap-7.x-3.10.tar.gz  \
ls *.gz | xargs -i tar zxvf {}
```

###Clean Urls
Apache Conf :
```
Options Indexes FollowSymLinks
AllowOverride None
Require all granted
RewriteEngine On
RewriteBase /MY_FORK/
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_URI} !=/favicon.ico
RewriteRule ^ index.php [L]
``` 
###Installing rvm, npm, Ruby, Grunt (Developement environment)
- Install RVM for Ruby
```
sudo apt-get install zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 \
libxml2 libxml2-dev libxslt-dev gawk libgdbm-dev libncurses5-dev automake libtool bison libffi-dev nodejs \
curl -sSL https://get.rvm.io | bash -s stable
```
 - Install Ruby and gems
```
rvm install ruby-2.4.0
sudo apt-get install gem
gem install sass
gem install compass
```
 - Install npm
```
sudo apt-get install nodejs npm
```
 - Install grunt 
```
sudo npm install -g grunt-cli
```
> More info on Grunt Critical : https://github.com/bezoerb/grunt-critical
###SASS (compass)
 - Copy Starter kit and download last sources
```
cp -r sites/all/themes/bootstrap/starterkits/sass/* sites/all/themes/MY_THEME
wget https://github.com/twbs/bootstrap-sass/archive/v3.3.7.tar.gz
tar -zxvf v3.3.7.tar.gz
mv bootstrap-sass-3.3.7/ bootstrap
```
 - copy MY_THEME.info and update it
```
cp sass.starterkit MY_THEME.info
```
 - create config.rb (https://www.drupal.org/node/2181845)

 - compass
```
compass init
```
 - add following lines to theme .info file : 
```
stylesheets[all][] = css/print.css
stylesheets[all][] = css/screen.css
stylesheets-conditional[IE][all][] = css/ie.css
```
 - Add following line in sites/all/themes/SUB_THEME/config.rb
> `add_import_path "../webstar/scss"`

###Caching issues
Two cookies prevent proper page caching
 - `has_js` which can be removed with Force JS module