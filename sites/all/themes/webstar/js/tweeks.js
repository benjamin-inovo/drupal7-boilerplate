/*
 * Fix scrolling issues with bootstrap fixed top navbar
 */

var shift = 0;

var shiftWindow = function() {
	window.scrollBy(0, shift)
};

window.addEventListener("hashchange", shiftWindow);

jQuery(window).on('load', function() {
	var bodyClass = jQuery('body').attr('class').split(" ");
	if (bodyClass.indexOf('navbar-is-fixed-top') !== -1)
		shift -= 40;
	// When logged-in
	if (bodyClass.indexOf('toolbar') !== -1) {
		shift -= 30;
	}

	// Remove cookies on logout
	if (Drupal.toolbar) {
		jQuery('a[href*="user\/logout"]').click(function() {
			Cookies.remove('Drupal.toolbar.collapsed', {
				path : Drupal.settings.basePath
			});
			Cookies.remove('has_js');
		});
	}

});

/*
 * Fix problem with bootstrap menu not closing on small devices after clicking
 * an item
 */

jQuery(document).on(
		'click',
		'.navbar-collapse.in',
		function(e) {
			if (jQuery(e.target).is('a')
					&& jQuery(e.target).attr('class') != 'dropdown-toggle') {
				jQuery(this).collapse('hide');
			}
		});
