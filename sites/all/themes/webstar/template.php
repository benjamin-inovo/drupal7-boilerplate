<?php
/*
 * Hooks are in order of execution
 *
 */
/**
 *
 * @param unknown $variables        	
 */
function webstar_preprocess_html(&$variables) {
}
/**
 *
 * @param unknown $variables        	
 */
function webstar_preprocess_node(&$variables) {
	global $grid, $webstar;
	/*
	 * Array for boostrap layout
	 * key : number of elements per row
	 * value : element css class (string 'col-sm', 'col-lg'... will be added)
	 */
	$layout = array (
			1 => '12',
			2 => '6',
			3 => '4',
			4 => '3',
			5 => '2',
			6 => '2',
			7 => '1',
			8 => '1',
			9 => '1',
			10 => '1',
			11 => '1',
			12 => '1' 
	);
	
	if (isset ( $grid ) && array_key_exists ( $variables ['vid'], $grid )) {
		$variables ['classes_array'] [] = 'col-sm-' . $layout [$grid [$variables ['vid']] [1]];
		for($i = $grid [$variables ['vid']] [2]; $i > 0; $i --) {
			$variables ['classes_array'] [] = 'col-sm-offset-' . $layout [$grid [$variables ['vid']] [1]];
		}
		$variables ['bootstrap_position'] = $grid [$variables ['vid']] [0];
	}
	
	$l_aFields = array (
			'field_field_1',
			'field_field_2',
			'field_field_3',
			'field_field_4' 
	);
	
	foreach ( $l_aFields as $field ) {
		if (isset ( $variables [$field] ['und'] [0] ['value'] )) {
			if ($variables [$field] ['und'] [0] ['value'] != 'field_cta' || isset ( $variables ['field_cta_target'] [0] ['safe_value'] ))
				$variables ['bootstrap_order'] [] = $variables [$field] ['und'] [0] ['value'];
		}
	}
	
	/*
	 * Background Color for articles and sections
	 */
	if (isset ( $variables ['field_background_color'] ['und'] )) {
		$className = 'node-' . $variables ['nid'] . '-bg-color';
		$webstar ['css'] [$variables ['nid']] [] = ".$className { background-color: " . $variables ['field_background_color'] ['und'] [0] ['value'] . "; }";
		if (isset ( $variables ['field_section_background'] ))
			$variables ['webstar'] ['section_css'] [] = $className;
		else
			$variables ['classes_array'] [] = $className;
	}
	
	/*
	 * Background Image for articles and sections
	 */
	if (isset ( $variables ['field_background_image'] ['und'] ) && $variables ['field_background_image'] ['und'] [0] ['value'] == '1') {
		$className = 'node-' . $variables ['nid'] . '-bg-image';
		// $image = "data:image/jpeg;base64," . base64_encode ( file_get_contents ( drupal_realpath ( $variables ['field_image'] [0] ['uri'] ) ) );
		$image = file_create_url ( $variables ['field_image'] [0] ['uri'] );
		$webstar ['css'] [$variables ['nid']] [] = ".$className { background-image: url('" . $image . "'); background-size: cover;}";
// 		$webstar ['css'] [$variables ['nid']] [] = "@media screen and (max-width: 768px) { .$className {background-size: auto 100% !important; background-position: 30% 5% !important;}}";
		if (isset ( $variables ['field_section_background'] ))
			$variables ['webstar'] ['section_css'] [] = $className;
		else
			$variables ['classes_array'] [] = $className;
	}
	
	/*
	 * Supress title for forms
	 */
	if ($variables ['type'] == 'webform')
		$variables ['title'] = '';
}

/**
 *
 * @param unknown $page        	
 */
function webstar_page_alter(&$page) {
	if (drupal_is_front_page () || $page ['#type'] == 'page') {
		unset ( $page ['sidebar_first'] );
		unset ( $page ['sidebar_second'] );
	}
}

/**
 * Pre-processes variables for the "page" theme hook.
 *
 * See template for list of available variables.
 *
 * @see page.tpl.php @ingroup theme_preprocess
 */
function webstar_preprocess_page(&$variables) {
	global $webstar;
	
	if (! variable_get ( 'DEV', false )) {
		drupal_add_js ( drupal_get_path ( 'theme', 'webstar' ) . '/js/ga.js' );
		$variables ['scripts'] = drupal_get_js (); // necessary in D7?
	}
	
	if ($variables ['page'] ['#type'] == 'page') {
		$variables ['breadcrumb'] = '';
		$variables ['title'] = '';
	}
	
	// remove user pages from search engines index
	$url_parts = explode ( '/', request_path () );
	
	if ($url_parts [0] == 'user') {
		drupal_add_html_head ( array (
				'#type' => 'html_tag',
				'#tag' => 'meta',
				'#attributes' => array (
						'name' => 'robots',
						'content' => 'noindex' 
				) 
		), 'no_index_rsvp' );
	}
	
	/*
	 * CSS files
	 */
	$css_path = drupal_get_path ( 'theme', $GLOBALS ['theme'] ) . '/css/';
	
	// If needed put additionnal styles in your sub theme Gruntfile.js file inside critical options
	$critical_css = $css_path;
	if (variable_get ( 'DEV', false )) {
		$critical_css .= 'dev-';
	}
	$critical_css .= ($variables ['is_front'] == TRUE) ? 'critical-home.css' : 'critical-all.css';

	$css_includes = array (
			array (
					'file' => $critical_css,
					'options' => array (
							'type' => 'inline',
							'scope' => 'header',
							'group' => CSS_SYSTEM,
							'weight' => - 1000,
							'preprocess' => FALSE 
					) 
			) 
	);
	
	/*
	 * Update dynamics css file
	 */
	
	if (isset ( $webstar ['css'] )) {
		foreach ( $webstar ['css'] as $css_nid => $css_classes ) {
			$css_dynamic_file = $css_path . 'pages/node-' . $css_nid . '.css';
			
			$css_includes [] = array (
					'file' => $css_dynamic_file,
					'options' => array () 
			);
			$fileHandler = mod_webstar_file_handler ( 'node-' .$css_nid, $css_includes [count ( $css_includes ) - 1] ['file'], "w" );
			if ($fileHandler) {
				fwrite ( $fileHandler, implode ( '', $css_classes ) );
			}
		}
	}
	
	// Add CSS files
	foreach ( $css_includes as $css_file ) {
		if (isset ( $css_file ['file'] ) && file_exists ( $css_file ['file'] )) {
			if (isset ( $css_file ['options'] ['type'] ) && $css_file ['options'] ['type'] == 'inline')
				$content = file_get_contents ( $css_file ['file'] );
			else
				$content = $css_file ['file'];
			drupal_add_css ( $content, $css_file ['options'] );
		}
	}
}

/**
 *
 * @param unknown $variables        	
 */
function webstar_process_page(&$variables) {
}
function webstar_form_alter(array &$form, array &$form_state = array(), $form_id = NULL) {
	if ($form_id) {
		switch ($form_id) {
			case 'webform_client_form_10' :
				$form ['actions'] ['reset'] = array (
						'#type' => 'button',
						'#button_type' => 'reset',
						'#value' => t ( 'Annuler' ),
						'#weight' => 20,
						'#attributes' => array (
								'onclick' => 'this.form.reset(); return false;',
								'class' => array (
										'btn-default',
										'pull-right' 
								) 
						) 
				);
				$form ['actions'] ['submit'] ['#attributes'] ['class'] [] = 'pull-right';
				$form ['actions'] ['submit'] ['#attributes'] ['class'] [] = 'btn-primary';
				break;
		}
	}
}
function webstar_form_process_actions($element, &$form_state, &$form) {
	$element ['#attributes'] ['class'] [] = 'col-sm-11 col-sm-offset-1';
	return $element;
}
function webstar_status_messages($variables) {
	$display = $variables ['display'];
	$output = '';
	
	$status_heading = array (
			'status' => t ( 'Status message' ),
			'error' => t ( 'Error message' ),
			'warning' => t ( 'Warning message' ),
			'info' => t ( 'Informative message' ) 
	);
	
	// Map Drupal message types to their corresponding Bootstrap classes.
	// @see http://twitter.github.com/bootstrap/components.html#alerts
	$status_class = array (
			'status' => 'success',
			'error' => 'danger',
			'warning' => 'warning',
			// Not supported, but in theory a module could send any type of message.
			// @see drupal_set_message()
			// @see theme_status_messages()
			'info' => 'info' 
	);
	
	// Retrieve messages.
	$message_list = drupal_get_messages ( $display );
	
	// Allow the disabled_messages module to filter the messages, if enabled.
	if (module_exists ( 'disable_messages' ) && variable_get ( 'disable_messages_enable', '1' )) {
		$message_list = disable_messages_apply_filters ( $message_list );
	}
	
	foreach ( $message_list as $type => $messages ) {
		$class = (isset ( $status_class [$type] )) ? ' alert-' . $status_class [$type] : '';
		$output .= "<div class=\"alert alert-block$class messages $type\">\n";
		$output .= "  <a class=\"close\" data-dismiss=\"alert\" href=\"#\">&times;</a>\n";
		
		if (! empty ( $status_heading [$type] )) {
			if (! module_exists ( 'devel' )) {
				$output .= '<h4 class="element-invisible">' . _bootstrap_filter_xss ( $status_heading [$type] ) . "</h4>\n";
			} else {
				$output .= '<h4 class="element-invisible">' . $status_heading [$type] . "</h4>\n";
			}
		}
		
		if (count ( $messages ) > 1) {
			$output .= " <ul>\n";
			foreach ( $messages as $message ) {
				if (! module_exists ( 'devel' )) {
					$output .= '  <li>' . _bootstrap_filter_xss ( $message ) . "</li>\n";
				} else {
					$output .= ' <li>' . $message . "</li>\n";
				}
			}
			$output .= " </ul>\n";
		} else {
			if (! module_exists ( 'devel' )) {
				$output .= _bootstrap_filter_xss ( $messages [0] );
			} else {
				$output .= $messages [0];
			}
		}
		$output .= "</div>\n";
	}
	return $output;
}