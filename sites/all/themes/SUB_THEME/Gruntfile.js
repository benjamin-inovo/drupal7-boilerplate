module.exports = function(grunt) {
	var host_dev = 'http://localhost/drupal7-boilerplate/';
	var host_prod = 'http://PRODUCTION_SERVER/';

	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		watch : {
			src : {
				files : [ 'scss/*.scss' ],
				tasks : [ 'compass:dist', 'critical' ]
			},
		},
		compass : {
			dist : {
				options : {
					config : 'config.rb'
				}
			}
		},
		critical : {
			home : {
				options : {
					base : './',
					dimensions : [ {
						height : 568,
						width : 320
					}, {
						height : 768,
						width : 1366
					} ],
					timeout : 60000,
					inlineImages : true,
				},
				src : host_dev,
				dest : 'css/dev-critical-home.css'
			},
			all : {
				options : {
					base : './',
					dimensions : [ {
						height : 568,
						width : 320
					}, {
						height : 768,
						width : 1366
					} ],
					timeout : 60000,
					inlineImages : true,
				},
				src : host_dev + 'user',
				dest : 'css/dev-critical-all.css'
			},
		},
		shell : {
			prodCritical : {
				command : [ "for file in css/dev-*; do cp -a $file css/${file#*-}; sed -i 's/" + host_dev.replace(/http:\/\//, '').replace(/[\/:]/g, '\\$&') + "/" + host_prod.replace(/http:\/\//, '').replace(/[\/:]/g, '\\$&') + "/g' css/${file#*-}; done", 'ls -l css/*critical*' ].join('&&')
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-critical');
	grunt.registerTask('default', ['critical', 'shell:prodCritical']);
}
